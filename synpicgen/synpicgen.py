#!/usr/bin/env python3

import argparse
import re

from functools import reduce
from pathlib import Path
from sys import argv, version_info
from urllib.request import urlopen
from urllib.error import URLError


OPTS_UNWANTED = ["--config", "--help"]
ONLINE_PICOME_ASCIIDOC = "https://raw.githubusercontent.com/yshui/picom/\
next/man/picom.1.asciidoc"
PLACEHOLDER_OPTION = "{{{OPTIONS}}}"
TIMEOUT = 5


class ListTools:
    """Groups together functions useful for list management"""
    @staticmethod
    def remove_duplicate(_list: list) -> list:
        """Remove duplicate(s) item(s) from list `_list` and 
        returns a new list"""
        return reduce(lambda i0, i1: i0 + [i1]
                      if i1 not in i0 else i0, _list, [])

    @staticmethod
    def remove_unwanted(_list: list, _unwanted: list = OPTS_UNWANTED) -> list:
        """Remove unwanted item(s) listed in `_unwanted` from list `_list` and
        returns a new list"""
        return list(filter(lambda i: i not in _unwanted, _list))

    @staticmethod
    def remove_hyphen(_list: list) -> list:
        """Remove '--' from item starting with '--' in list `_list`"""
        return list(map(lambda i: i[2:] if i[:2] == "--" else i, _list))


class BlueprintTools:
    def __init__(self, bp, outfile=None):
        if Path(bp).is_file():
            self.bp = bp
        else:
            raise FileNotFoundError
        if outfile:
            self.outfile = outfile


#        self._plhold_opts = r"{{{OPTIONS}}}"

    def _writable_output(self, infile: str = None) -> bool:
        """Returns `True` if `infile` is writable"""
        try:
            Path(infile).write_text("")
        except PermissionError:
            return False
        except FileNotFoundError:
            return False
        else:
            return True

    def _replace_placeholder(self, plhold: str, repl: str) -> str:
        """Reads file `self.bp` line by line and replaces placeholder `_plhold`
        by its replacement `_repl`. Returns the new file content as `str`"""
        func_replace_plhold = lambda x: x.replace(plhold, repl)
        with open(self.bp, mode="r") as _file:
            lines = _file.readlines()
            return "".join(list(map(func_replace_plhold, lines)))

    def _write_new_content_in_file(self, content: str, outfile: str) -> bool:
        """Returns True if new `content` is written in file `outfile`"""
        if self._writable_output(outfile):
            with open(outfile, mode="w") as _file:
                _file.write(content)
                return True
        return False

    def replace_placeholder_in_file(self, _plhold: str, _repl: str) -> bool:
        """Read file `bp` line by line and if it finds the placehold `_plhold`
        in the line it replaces it with `_repl` string"""
        func_replace_plhold = lambda x: x.replace(_plhold, _repl)
        with open(self.bp, mode="r") as _file:
            lines = _file.readlines()
        with open(self.bp, mode="w") as _file:
            _file.write("".join(list(map(func_replace_plhold, lines))))


class MiscTools:
    @staticmethod
    def commandline_interface() -> argparse.Namespace:
        """Sets arguments for CLI"""
        parser = argparse.ArgumentParser()
        parser.add_argument("-i", "--in-place", action="store_true")
        parser.add_argument("-o", "--output", default="")
        parser.add_argument("blueprint", default="")
        return parser.parse_args()


class FetchOnlinePicomDoc:
    def __init__(self, asciidoc: str = "", timeout: int = 5) -> None:
        if asciidoc != "":
            self.asciidoc = asciidoc
        elif 'ONLINE_PICOME_ASCIIDOC' in globals():
            self.asciidoc = ONLINE_PICOME_ASCIIDOC
        self.timeout = timeout
        self._re_picom_opts = re.compile(r"--[a-z]+[a-z-]+")

    def is_online_doc_reachable(self, url: str = "", timeout: int = 5) -> bool:
        """Returns True if picom asciidoc is reachable"""
        if not url:
            url = self.asciidoc
        try:
            with urlopen(url, timeout=TIMEOUT) as resp:
                if resp.status == 200:
                    return True
        except URLError:
            raise URLError("Has picom's asciidoc been moved ?")

    def get_online_asciidoc(self, url: str = ONLINE_PICOME_ASCIIDOC) -> str:
        """Returns asciidoc as a string"""
        with urlopen(url, timeout=TIMEOUT) as pic:
            return pic.read().decode("utf-8")

    def get_opts(self, asciidoc: str) -> list:
        """Returns asciidoc options as a list"""
        return self._re_picom_opts.findall(asciidoc)

    def get_wintypes(self, asciidoc: str) -> list:
        """Read `asciidoc` and returns window-type-specific settings as a list"""
        re_find_wintypes = re.compile(r"wintypes:\n\{\s+?WINDOW_TYPE.*\n\}\;")
        re_get_wintypes = re.compile(r"\s([a-z-]+)\s=\s\w+;")
        return re_get_wintypes.findall(re_find_wintypes.findall(asciidoc)[0])


if __name__ == "__main__":
    args = MiscTools.commandline_interface()
    if args.blueprint == "unittest":
        from sys import argv
        del (argv[1:])
        unittest.main()
    if version_info <= (3, 8):
        print("This script must be run with a version of Python > 3.8")
    if args.output and args.in_place:
        print("--output and --in-place are mutuables exclusives")
        exit(1)

    # fetch picom's doc
    picom = FetchOnlinePicomDoc()
    if picom.is_online_doc_reachable():
        asciidoc = picom.get_online_asciidoc()
    # grab picom's option from doc
    opts = " ".join(
        ListTools.remove_duplicate(
            ListTools.remove_hyphen(
                ListTools.remove_unwanted(
                    picom.get_opts(asciidoc)))))

    if args.output:
        output = args.output
    elif args.in_place:
        output = args.blueprint

    bp = BlueprintTools(args.blueprint, output)
    content_with_opts = bp._replace_placeholder(PLACEHOLDER_OPTION, opts)
    bp._write_new_content_in_file(content_with_opts, output)
