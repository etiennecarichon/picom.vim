from synpicgen.synpicgen import ListTools, BlueprintTools, \
        FetchOnlinePicomDoc, urlopen, ONLINE_PICOME_ASCIIDOC

from urllib.error import URLError
import unittest
from unittest.mock import patch, MagicMock
from tempfile import NamedTemporaryFile


class TestListTools(unittest.TestCase):
    def test_remove_duplicate(self) -> None:
        _list = ["foo0", "foo1", "foo2"]
        self.assertEqual(ListTools.remove_duplicate(_list + ["foo1"]), _list)
        self.assertEqual(ListTools.remove_duplicate(_list + _list), _list)

    def test_remove_unwanted(self) -> None:
        _list, _unwanted = ["--foo", "--bar"], ["--oof", "--rab"]
        self.assertEqual(
            ListTools.remove_unwanted(_list + _unwanted, _unwanted), _list)

    def test_remove_hyphen(self) -> None:
        _list = ["--foo", "--bar"]
        self.assertEqual(ListTools.remove_hyphen(_list), ["foo", "bar"])


class TestBlueprints(unittest.TestCase):
    def setUp(self) -> None:
        self._tmpfile = NamedTemporaryFile()
        self.bp = self._tmpfile.name

    def tearDown(self) -> None:
        self._tmpfile.close()

    def test_blueprint_found(self) -> None:
        blueprint = BlueprintTools(self.bp)
        self.assertEqual(blueprint.bp, self.bp)

    def test_blueprint_notfound(self) -> None:
        with self.assertRaises(FileNotFoundError):
            blueprint = BlueprintTools("foofile")

    def test_replace_placeholder(self) -> None:
        tmpfile = NamedTemporaryFile()
        base_content = "syntax keyword Keyword "
        plhold = "{{{OPTION}}}"
        repl = "foobar"

        with open(tmpfile.name, mode="w") as bp:
            bp.write(base_content + plhold)
        with open(tmpfile.name, mode="r") as _bp:
            blueprint = BlueprintTools(tmpfile.name)
            new_content = blueprint._replace_placeholder(plhold, repl)
            self.assertEqual(new_content, base_content + repl)

    def test_write_new_content_in_file(self) -> None:
        tmpfile = NamedTemporaryFile()
        content = "foobar"
        blueprint = BlueprintTools(self.bp, tmpfile.name)
        blueprint._write_new_content_in_file(content, blueprint.outfile)
        with open(tmpfile.name, mode="r") as _file:
            self.assertEqual(_file.read(), content)

    def test_blueprint_repl(self) -> None:
        tmpfile = NamedTemporaryFile()
        pl, repl = "{{{OPTIONS}}}", "foobar"
        line = "syntax keyword Keyword "

        with open(tmpfile.name, mode="w") as _bp:
            _bp.write(line + pl)
        with open(tmpfile.name, mode="r") as _bp:
            blueprint = BlueprintTools(tmpfile.name)
            blueprint.replace_placeholder_in_file(pl, repl)
            self.assertEqual(_bp.read(), line + repl)
        tmpfile.close()

    def test_writable_output(self) -> None:
        blueprint = BlueprintTools(self.bp, self._tmpfile.name)
        self.assertTrue(blueprint._writable_output(blueprint.outfile))

    def test_nonwritable_output(self) -> None:
        blueprint = BlueprintTools(self.bp, "/unsdtd/file/hierarchy")
        self.assertFalse(blueprint._writable_output(blueprint.outfile))
        # TODO: test PermissionError


class TestPicom(unittest.TestCase):
    def setUp(self) -> None:
        self.onlinepicom = FetchOnlinePicomDoc()
        self.cm = MagicMock()
        self.cm.status = 200
        self.cm.read.return_value = 'foobar'
        self.cm.__enter__.return_value = self.cm

    def test_asciidoc(self) -> None:
        p = FetchOnlinePicomDoc()
        self.assertEqual(p.asciidoc, ONLINE_PICOME_ASCIIDOC)
        del (p)
        p2 = FetchOnlinePicomDoc("asciifoo")
        self.assertEqual(p2.asciidoc, "asciifoo")

    @patch('synpicgen.synpicgen.urlopen', side_effect=URLError(""))
    def test_online_doc_unreachable(self, mock_urlopen) -> None:
        with self.assertRaises(URLError):
            self.onlinepicom.is_online_doc_reachable()

    @patch('synpicgen.synpicgen.urlopen')
    def test_online_doc_reachable(self, mock_urlopen) -> None:
        mock_urlopen.return_value = self.cm
        self.assertTrue(self.onlinepicom.is_online_doc_reachable())

    def test_get_options(self) -> None:
        content = """Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ac
        ut consequat semper viverra nam libero justo laoreet. Maecenas sed enim
        ut sem viverra aliquet. Id diam vel quam --elementum pulvinar etiam non
        quam. Gravida neque convallis a cras semper auctor --neque vitae
        tempus. Aenean sed adipiscing diam donec --adipiscing tristique risus
        nec feugiat. Urna porttitor rhoncus dolor purus non enim praesent.
        Pellentesque id nibh tortor id. Tincidunt eget --nullam non nisi.
        Imperdiet sed euismod nisi porta lorem. Etiam tempor orci eu lobortis
        elementum nibh tellus."""
        opts_list = ["--elementum", "--neque", "--adipiscing", "--nullam"]
        picom = FetchOnlinePicomDoc()
        self.assertEqual(picom.get_opts(content), opts_list)
